
<div class="col-12 col-md-3" style="background-color:cornflowerblue">
    <nav class="navbar navbar-expand-md navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavLeft" aria-controls="navbarNavLeft" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavLeft">
            <ul class="navbar-nav flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="#">Prima Pagina <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Cuvant Inainte <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Indrumari <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Cuprins <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">CV <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Testimoniale<i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Comanda <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Tstoria in imagini <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Istoria in clasa XI-A <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Subiecte rezolvate<i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</div>