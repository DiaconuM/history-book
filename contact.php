<html>
<!-- head-->
<?php include "parts/head.php" ?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<body style="background-color: grey">
<!-- HEADER-->
<div class="container">
    <?php include "parts/header.php" ?>
    <!-- SideBar-->
    <div class="row" style="background-color: cornflowerblue">
        <?php include "parts/sidebar.php" ?>
        <div class="col-12 col-md-9">
            <h2 class="text-left">Contact</h2>
<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <section class="resume-section text-left" id="contact">
                    <div class="my-auto">
                        <div class="form-group">
                            <input
                                type="name"
                                class="form-control"
                                placeholder="Nume"
                                name="name"
                                required
                            />
                        </div>
                        <div class="form-group ">
                            <input
                                type="email"
                                class="form-control"
                                placeholder="Email"
                                name="name"
                                required
                            />
                        </div>
                        <div class="form-group ">
              <textarea
                  class="form-control"
                  type="text"
                  placeholder="Mesaj"
                  rows="7"
                  name="name"
                  required
              ></textarea>
                        </div>

                        <button type="submit" class="btn btn-submit btn-info text-center">Trimite
                        </button>
                    </div>
                </section>
            </div>


            <script
                defer
                src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"
                integrity="sha384-0pzryjIRos8mFBWMzSSZApWtPl/5++eIfzYmTgBBmXYdhvxPc+XcFEk+zJwDgWbP"
                crossorigin="anonymous"
            ></script>
            </div>
        </div>

    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <b> Cartea se expediaza prin <font style="color: red">Posta Româna </font> !
                    Dupa ce ati trimis comanda, veti primi un <font
                        style="color: red">email</font> (in maxim doua zile) cu solicitarea
                    de a <font style="color: red">confirma comanda</font>.</b>
            </div>
        </div>
    </div>
</div>
    </div>

</div>
    <?php include "parts/footer.php";?>
</div>